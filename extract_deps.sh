#!/usr/bin/env bash
# pack the submodules of TIC-80 as an archive

die() {
	echo "[ERROR] $*"
	exit 1
}

info() {
	echo "[INFO] $*"
}

PN="${1}"
PV="${2}"
SRC_URI="${3}"
P="${PN}-${PV}"

tmpdir="$(mktemp -d)"
archivedir="${PWD}/dep-archives"

mkdir -p "${archivedir}" || die "Failed to create ${archivedir}"
cd "${tmpdir}" || die "cd ${tmpdir} failed"

info "Extracting submodules of ${P} from source ${SRC_URI}"

info "Cloning repository"
git clone --recursive "${SRC_URI}" || die "git failed"

pushd "${PN}" || die "Failed to enter ${PWD}/${PN}"

info "Finding submodules"
git checkout "v${PV}"
git config -z --file .gitmodules --get-regexp '\.path$' | \
	sed -nz 's/^[^\n]*\n//p' | \
	tr '\0' '\n' > modules-list.txt || die "Could not list submodules"

info "Creating archive"
tar -acvf "${P}"-deps.tar.xz -T modules-list.txt

mv "${P}"-deps.tar.xz "${archivedir}" || die "Failed to move ${P}-deps.tar.xz to archivedir"

popd || die
